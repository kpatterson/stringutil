#ifndef _KHPtech_StringUtil_CompilerFeatures_h
#define _KHPtech_StringUtil_CompilerFeatures_h
/*
 *  CompilerFeatures.h
 *  KHPtech Shared Components :: StringUtil
 *
 *  This version of StringUtil is hosted at:
 *  https://bitbucket.org/kpatterson/stringutil
 *
 *  Created by Kevin H. Patterson on 2013-02-08.
 *  Copyright (C) 2008-2013, Kevin H. Patterson. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Kevin H. Patterson, KHPtech, nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

// C++11 alias templates

#ifndef cxx_alias_templates
	#ifdef __has_feature
		#if __has_feature(cxx_alias_templates)
			#define cxx_alias_templates 1
		#endif
	#endif
	#if (__cplusplus > 201103L) || (__GNUC__ > 4) || ((__GNUC__ == 4) && (__GNUC_MINOR__ >= 7))
		#define cxx_alias_templates 1
	#endif

#endif

// C++11 default template arguments in function templates

#ifndef cxx_default_function_template_args
	#ifdef __has_feature
		#if __has_feature(cxx_default_function_template_args)
			#define cxx_default_function_template_args 1
		#endif
	#endif
	#if (__cplusplus > 201103L) || (__GNUC__ > 4) || ((__GNUC__ == 4) && (__GNUC_MINOR__ >= 4))
		#define cxx_default_function_template_args 1
	#endif
#endif


#endif //_KHPtech_StringUtil_CompilerFeatures_h
