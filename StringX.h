#ifndef _KHPtech_StringUtil_StringX_h
#define _KHPtech_StringUtil_StringX_h
/*
 *  StringX.h
 *  KHPtech Shared Components :: StringUtil
 *
 *  This version of StringUtil is hosted at:
 *  https://bitbucket.org/kpatterson/stringutil
 *
 *  Created by Kevin H. Patterson on 2013-01-30.
 *  Copyright (C) 2008-2013, Kevin H. Patterson. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Kevin H. Patterson, KHPtech, nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace KHP {

	template<class T = custom_string_t, class S = custom_stringstream_t, class V = custom_vector_t<T>, class M = custom_map_t<T,T> >
	class StringX
	: public T
	{
	public:
		StringX()
		: T()
		{}

		StringX( const char* i_CString )
		: T( i_CString )
		{}

		StringX( const char* i_CBuffer, typename T::size_type i_Length )
		: T( i_CBuffer, i_Length )
		{}

		StringX( const StringX& i_CopySource )
		: T( i_CopySource )
		{}

		StringX( const T& i_CopySource )
		: T( i_CopySource )
		{}

		/// Find the first whitespace character searching forward
		typename T::size_type find_white( typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 ) const
		{ return KHP::find_white<T>( *this, i_StartPos, i_EndPos ); }

		/// Find the first whitespace character searching backward
		typename T::size_type find_white_r( typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 ) const
		{ return KHP::find_white_r<T>( *this, i_StartPos, i_EndPos ); }

		/// Find the first non-whitespace character searching forward
		typename T::size_type find_black( typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 ) const
		{ return KHP::find_black<T>( *this, i_StartPos, i_EndPos ); }

		/// Find the first non-whitespace character searching backward
		typename T::size_type find_black_r( typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 ) const
		{ return KHP::find_black_r<T>( *this, i_StartPos, i_EndPos ); }

		/// Trim whitespace from the left side of a string
		T ltrim() const
		{ return KHP::ltrim<T>( *this ); }

		/// Trim whitespace from the right side of a string
		T rtrim() const
		{ return KHP::rtrim<T>( *this ); }

		/// Trim whitespace from both sides of a string
		T trim() const
		{ return KHP::trim<T>( *this ); }

		/// Convert string to uppercase
		T ucase() const
		{ return KHP::ucase<T>( *this ); }

		/// Convert string to lowercase
		T lcase() const
		{ return KHP::lcase<T>( *this ); }

		/// Test string for alpha-only content
		bool IsAlpha( typename T::size_type i_Start = 0, typename T::size_type i_Length = T::npos ) const
		{ return KHP::IsAlpha<T>( *this, i_Start, i_Length ); }

		/// Test string for alphanumeric-only content
		bool IsAlphaNum( typename T::size_type i_Start = 0, typename T::size_type i_Length = T::npos ) const
		{ return KHP::IsAlphaNum<T>( *this, i_Start, i_Length ); }

		/// Convert an int to a string
		void FromInt( int i_Value )
		{ *this = KHP::IntToString<T,S>( i_Value ); }

		/// Convert a string to an int
		int ToInt() const
		{ return KHP::StringToInt<T,S>( *this ); }

		/// Convert an unsigned int to a string
		void FromUInt( unsigned int i_Value )
		{ *this = KHP::UIntToString<T,S>( i_Value ); }

		/// Convert a string to an unsigned int
		unsigned int ToUInt() const
		{ return KHP::StringToInt<T,S>( *this ); }

		/// Convert a float to a string
		void FromFloat( float i_Value )
		{ *this = KHP::FloatToString<T,S>( i_Value ); }

		/// Convert a string to a float
		float ToFloat() const
		{ return KHP::StringToFloat<T,S>( *this ); }

		/// Convert a double to a string
		void FromDouble( double i_Value )
		{ return KHP::DoubleToString<T,S>( i_Value ); }

		/// Convert a string to a double
		double ToDouble() const
		{ return KHP::StringToDouble<T,S>( *this ); }

		/// Escape string appropriately for XML text
		T ToXMLText() const
		{ return KHP::EscapeXMLText<T>( *this ); }

		/// Escape string appropriately for XML attribute value
		T ToXMLAttrib() const
		{ return KHP::EscapeXMLAttrib<T>( *this ); }

		/// Convert raw data bytes to a string representation of hexadecimal digits
		T ToHex() const
		{ return KHP::DataToStringHex( *this, T::size() ); }

		/// Convert raw data bytes to a string representation of binary digits
		T ToBinary() const
		{ return KHP::DataToStringBinary( *this, T::size() ); }

		/// Tokenize based on 1 or more single-character delimiters
		int tokenize( V& o_Tokens, const T& i_Delimiters, const int i_Limit = 0 ) const
		{ return KHP::tokenize<T,V>( *this, o_Tokens, i_Delimiters, i_Limit ); }

		/// Tokenize based on 1 or more single-character delimiters, returning a vector of tokens
		V tokenize( const T& i_Delimiters, const int i_Limit = 0 ) const
		{ V vec; KHP::tokenize<T,V>( *this, vec, i_Delimiters, i_Limit ); return vec; }

		/// Same as tokenize, but tokenizes tokens in reverse order
		int tokenize_r( V& o_Tokens, const T& i_Delimiters, int i_Limit = 0 ) const
		{ return KHP::tokenize_r<T,V>( *this, o_Tokens, i_Delimiters, i_Limit ); }

		/// Same as tokenize, but tokenizes tokens in reverse order, returning a vector of tokens
		V tokenize_r( const T& i_Delimiters, int i_Limit = 0 ) const
		{ V vec; KHP::tokenize_r<T,V>( *this, vec, i_Delimiters, i_Limit ); return vec; }

		/// Similar to Tokenize, but retains delimiter(s) on left side of each token
		int lsplit( V& o_Tokens, const T& i_Delimiters, const int i_Limit = 0 ) const
		{ return KHP::lsplit<T,V>( *this, o_Tokens, i_Delimiters, i_Limit ); }

		/// Similar to Tokenize, but retains delimiter(s) on left side of each token, returning a vector of tokens
		V lsplit( const T& i_Delimiters, const int i_Limit = 0 ) const
		{ V vec; return KHP::lsplit<T,V>( *this, vec, i_Delimiters, i_Limit ); return vec; }

		/// Same as lsplit, but splits tokens in reverse order
		int lsplit_r( V& o_Tokens, const T& i_Delimiters, int i_Limit = 0 ) const
		{ return KHP::lsplit_r<T,V>( *this, o_Tokens, i_Delimiters, i_Limit ); }

		/// Same as lsplit, but splits tokens in reverse order, returning a vector of tokens
		V lsplit_r( const T& i_Delimiters, int i_Limit = 0 ) const
		{ V vec; KHP::lsplit_r<T,V>( *this, vec, i_Delimiters, i_Limit ); return vec; }

		/// Tokenize based on a single, 1-or-more character delimiter
		int explode( V& o_Tokens, const T& i_Delimiter, const int i_Limit = 0 ) const
		{ return KHP::explode<T,V>( *this, o_Tokens, i_Delimiter, i_Limit ); }

		/// Tokenize based on a single, 1-or-more character delimiter, returning a vector of tokens
		V explode( const T& i_Delimiter, const int i_Limit = 0 ) const
		{ V vec; KHP::explode<T,V>( *this, vec, i_Delimiter, i_Limit ); return vec; }

		/// Same as explode, but tokenizes tokens in reverse order
		int explode_r( V& o_Tokens, const T& i_Delimiter, int i_Limit = 0 ) const
		{ return KHP::explode_r<T,V>( *this, o_Tokens, i_Delimiter, i_Limit ); }

		/// Same as explode, but tokenizes tokens in reverse order, returning a vector of tokens
		V explode_r( const T& i_Delimiter, int i_Limit = 0 ) const
		{ V vec; KHP::explode_r<T,V>( *this, vec, i_Delimiter, i_Limit ); return vec; }

		/// Combine tokens using a single, 1-or-more character delimiter
		void implode( const V& i_Tokens, const T& i_Delimiter = "", int i_Limit = 0 )
		{ *this = KHP::implode<T,V>( i_Tokens, i_Delimiter, i_Limit ); }

		/// Same as implode, but combines tokens in reverse order
		void implode_r( const V& i_Tokens, const T& i_Delimiter = "", int i_Limit = 0 )
		{ *this = KHP::implode_r<T,V>( i_Tokens, i_Delimiter, i_Limit ); }

		// Extract all tokens between i_StartDelimiter and i_EndDelimiter
		int extract( V& o_Tokens, const T& i_StartDelimiter, const T& i_EndDelimiter, int i_Limit = 0 )
		{ return KHP::extract( *this, o_Tokens, i_StartDelimiter, i_EndDelimiter, i_Limit ); }

		// Extract all tokens between i_StartDelimiter and i_EndDelimiter, returning a vector of tokens
		V extract( const T& i_StartDelimiter, const T& i_EndDelimiter, int i_Limit = 0 )
		{ V vec; KHP::extract( *this, vec, i_StartDelimiter, i_EndDelimiter, i_Limit ); return vec; }

		/// Parse a key-value pair, where key and value are separated by i_Joiner
		int ParsePair( M& o_Pairs, const T& i_Joiner ) const
		{ return KHP::ParsePair( *this, o_Pairs, i_Joiner ); }

		/// Parse a key-value pair, where key and value are separated by i_Joiner, returning a map of pairs
		M ParsePair( const T& i_Joiner ) const
		{ M pmap; KHP::ParsePair( *this, pmap, i_Joiner ); return pmap; }

		/// Parse key-value pairs, where key and value are separated by i_Joiner and pairs are separated by i_Delimiter
		int ParsePairs( M& o_Pairs, const T& i_Delimiter, const T& i_Joiner, int i_Limit = 0 ) const
		{ return KHP::ParsePairs( *this, o_Pairs, i_Delimiter, i_Joiner, i_Limit ); }

		/// Parse key-value pairs, where key and value are separated by i_Joiner and pairs are separated by i_Delimiter, returning a map of pairs
		M ParsePairs( const T& i_Delimiter, const T& i_Joiner, int i_Limit = 0 ) const
		{ M pmap; KHP::ParsePairs( *this, pmap, i_Delimiter, i_Joiner, i_Limit ); return pmap; }

	};

}

#endif // _StringUtil_StringX_h
