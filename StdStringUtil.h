#ifndef _KHPtech_StringUtil_StdStringUtil_h
#define _KHPtech_StringUtil_StdStringUtil_h
/*
 *  StdStringUtil.h
 *  KHPtech Shared Components :: StringUtil
 *
 *  This version of StringUtil is hosted at:
 *  https://bitbucket.org/kpatterson/stringutil
 *
 *  Created by Kevin H. Patterson on 2013-01-28.
 *  Copyright (C) 2008-2013, Kevin H. Patterson. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Kevin H. Patterson, KHPtech, nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <sstream>
#include <vector>
#include <map>

#include "StringUtil/CompilerFeatures.h"

namespace KHP {

#if (cxx_alias_templates == 1)
//NOTE: For compilers supporting template aliases (C++11), do it like this:

	using custom_string_t = std::string;
	using custom_stringstream_t = std::stringstream;
	template<class T> using custom_vector_t = std::vector<T>;
	template<class T, class TT> using custom_map_t = std::map<T,TT>;

#else
//NOTE: For compilers NOT supporting template aliases (C++03, etc.), do it like this:

	typedef std::string custom_string_t;
	typedef std::stringstream custom_stringstream_t;
	#define custom_vector_t std::vector
	#define custom_map_t std::map

//RANT: Yes, macros are evil. Upgrade to a C++11-compliant compiler.
#endif
	
}

#include "StringUtil/StringUtil.h"

#endif // ifndef _KHPtech_StringUtil_StdStringUtil_h
