/*
 *  demo.cpp
 *  KHPtech Shared Components :: StringUtil
 *
 *  This version of StringUtil is hosted at:
 *  https://bitbucket.org/kpatterson/stringutil
 *
 *  Created by Kevin H. Patterson on 12/26/08.
 *  Copyright (C) 2008-2013, Kevin H. Patterson. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Kevin H. Patterson, KHPtech, nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include "StdStringUtil.h"
#include "StringX.h"

using namespace std;

int main() {
	char z[33];
	for(int i=0; i<33;++i) z[i]=0;
	string zs( z, 32 );
	cout << zs.size() << ": " << KHP::DataToStringHex( (unsigned char*)zs.data(), zs.size() ) << endl;
	zs = KHP::rtrim( zs );
	cout << zs.size() << ": " << KHP::DataToStringHex( (unsigned char*)zs.data(), zs.size() ) << endl;

	cout <<   "   012345678901234567890123456789" << endl;
	const string s = "   A=1;b=2; C=3; d=4 ; E =35  ";
	cout << "s=\"" << s << "\"" << endl << endl;

	cout << "find_white(s): " << KHP::find_white( s ) << endl;
	cout << "find_white_r(s): " << KHP::find_white_r( s ) << endl << endl;
	cout << "find_white(s,3): " << KHP::find_white( s, 3 ) << endl;
	cout << "find_white_r(s,27): " << KHP::find_white_r( s, 27 ) << endl << endl;

	cout << "find_black(s): " << KHP::find_black( s ) << endl;
	cout << "find_black_r(s): " << KHP::find_black_r( s ) << endl << endl;

	cout << "ltrim(s): \"" << KHP::ltrim( s ) << "\"" << endl;
	cout << "rtrim(s): \"" << KHP::rtrim( s ) << "\"" << endl;
	cout << "trim(s):  \"" << KHP::trim( s ) << "\"" << endl << endl;

	cout << "ucase(s): \"" << KHP::ucase( s ) << "\"" << endl;
	cout << "lcase(s): \"" << KHP::lcase( s ) << "\"" << endl << endl;

	cout << "IsAlpha(s,3,1):     " << KHP::IsAlpha( s, 3, 1 ) << endl;
	cout << "IsAlphaNum(s,26,2): " << KHP::IsAlphaNum( s, 26, 2 ) << endl << endl;

	{
		vector<string> Tokens;
		KHP::tokenize( s, Tokens, string( ";=" ) );
		cout << "tokenize(s,Tokens,\";=\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::tokenize_r( s, Tokens, string( ";=" ) );
		cout << "tokenize_r(s,Tokens,\";=\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::lsplit( s, Tokens, string( ";=" ) );
		cout << "lsplit(s,Tokens,\";=\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::lsplit_r( s, Tokens, string( ";=" ) );
		cout << "lsplit_r(s,Tokens,\";=\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::explode( s, Tokens, string( ";" ) );
		cout << "explode(s,Tokens,\";\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::explode_r( s, Tokens, string( ";" ) );
		cout << "explode_r(s,Tokens,\";\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::explode( s, Tokens, string( ";" ) );
		const string Result = KHP::implode( Tokens, string( "+" ) );
		cout << "implode(Tokens,\"+\"): \"" << Result << "\"" << endl << endl;
	}

	{
		vector<string> Tokens;
		KHP::explode( s, Tokens, string( ";" ) );
		const string Result = KHP::implode_r( Tokens, string( "+" ) );
		cout << "implode_r(Tokens,\"+\"): \"" << Result << "\"" << endl << endl;
	}

	{
		string Copy = s;
		vector<string> Tokens;
		KHP::extract( Copy, Tokens, string( "=" ), string( ";" ) );
		cout << "Copy=s" << endl;
		cout << "extract(Copy,\"=\",\";\"): " << endl;
		cout << "{";
		for( vector<string>::const_iterator i = Tokens.begin(); i != Tokens.end(); ) {
			cout << "\"" << *i << "\"";
			if( ++i != Tokens.end() ) cout << ",";
		}
		cout << "}" << endl;
		cout << "Copy: \"" << Copy << "\"" << endl << endl;
	}

	{
		map<string, string> Pairs;
		KHP::ParsePairs< string,map<string,string>,vector<string> >( s, Pairs, string( ";" ), string("=") );
		cout << "ParsePairs(s,\";\",\"=\"): " << endl;
		cout << "{" << endl;
		for( map<string, string>::const_iterator i = Pairs.begin(); i != Pairs.end(); ) {
			cout << "\"" << i->first << "\"=\"" << i->second << "\"";
			if( ++i != Pairs.end() ) cout << ",";
			cout << endl;
		}
		cout << "}" << endl << endl;
	}

	cout << "DataToStringHex(&s[0],30): " << KHP::DataToStringHex<string>( reinterpret_cast<const unsigned char*>( &s[0] ), 30 ) << endl << endl;
	cout << "DataToStringBinary(&s[0],30): " << KHP::DataToStringBinary<string>( reinterpret_cast<const unsigned char*>( &s[0] ), 30 ) << endl << endl;

	const double Cpi = 3.141592653589793 * 100.0;
	const string CpiStr = "314.1592653589793";
	const string NCpiStr = "-314.1592653589793";

	cout << "IntToString(-314.1592653589793):    " << KHP::IntToString<string,stringstream>( -Cpi ) << endl;
	cout << "UIntToString(314.1592653589793):     " << KHP::UIntToString<string,stringstream>( Cpi ) << endl;
	cout << "FloatToString(-314.1592653589793):  " << KHP::FloatToString<string,stringstream>( -Cpi ) << endl;
	cout << "DoubleToString(-314.1592653589793): " << KHP::DoubleToString<string,stringstream>( -Cpi ) << endl;

	cout << "StringToInt(\"-314.1592653589793\"):    " << KHP::StringToInt<string,stringstream>( NCpiStr ) << endl;
	cout << "StringToUInt(\"314.1592653589793\"):     " << KHP::StringToUInt<string,stringstream>( CpiStr ) << endl;
	cout << "StringToFloat(\"-314.1592653589793\"):  " << KHP::StringToFloat<string,stringstream>( NCpiStr ) << endl;
	cout << "StringToDouble(\"-314.1592653589793\"): " << KHP::StringToDouble<string,stringstream>( NCpiStr ) << endl << endl;

	const string XMLTest = " \"Barnes & Noble\" <3 :> ";
	cout << "EscapeXMLText(\"" << XMLTest << "\"): " << KHP::EscapeXMLText( XMLTest ) << endl;
	cout << "EscapeXMLAttrib(\"" << XMLTest << "\"): " << KHP::EscapeXMLAttrib( XMLTest ) << endl << endl;

	return 0;
}
