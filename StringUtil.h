#ifndef _KHPtech_StringUtil_h
#define _KHPtech_StringUtil_h
/*
 *  StringUtil.h
 *  KHPtech Shared Components :: StringUtil
 *
 *  This version of StringUtil is hosted at:
 *  https://bitbucket.org/kpatterson/stringutil
 *
 *  Created by Kevin H. Patterson on 2008-12-26.
 *  Copyright (C) 2008-2013, Kevin H. Patterson. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Kevin H. Patterson, KHPtech, nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <time.h>

#include "StringUtil/CompilerFeatures.h"

//NOTE: Should we use default template parameters in our template functions? You decide.
// #define cxx_default_function_template_args 0
// #define cxx_default_function_template_args 1

// Interface
namespace KHP {
		
	/// Find the first whitespace character searching forward
	template<class T>
	typename T::size_type find_white( const T& i_Text, typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 );

	/// Find the first whitespace character searching backward
	template<class T>
	typename T::size_type find_white_r( const T& i_Text, typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 );

	/// Find the first non-whitespace character searching forward
	template<class T>
	typename T::size_type find_black( const T& i_Text, typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 );

	/// Find the first non-whitespace character searching backward
	template<class T>
	typename T::size_type find_black_r( const T& i_Text, typename T::size_type i_StartPos = 0, typename T::size_type i_EndPos = 0 );

	/// Trim whitespace from the left side of a string
	template<class T>
	T ltrim( const T& i_Text );
	
	/// Trim whitespace from the right side of a string
	template<class T>
	T rtrim( const T& i_Text );
	
	/// Trim whitespace from both sides of a string
	template<class T>
	T trim( const T& i_Text );

	/// Convert string to uppercase
	template<class T>
	T ucase( const T& i_Text );

	/// Convert string to lowercase
	template<class T>
	T lcase( const T& i_Text );

	/// Test string for alpha-only content
	template<class T>
	bool IsAlpha( const T& i_Text, typename T::size_type i_Start = 0, typename T::size_type i_Length = T::npos );

	/// Test string for alphanumeric-only content
	template<class T>
	bool IsAlphaNum( const T& i_Text, typename T::size_type i_Start = 0, typename T::size_type i_Length = T::npos );

	/// Convert an int to a string
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	T IntToString( int i_Value );

	/// Convert a string to an int
#if (cxx_default_function_template_args == 1)
	template<class T, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	int StringToInt( const T& i_Text );

	/// Convert an unsigned int to a string
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	T UIntToString( unsigned int i_Value );

	/// Convert a string to an unsigned int
#if (cxx_default_function_template_args == 1)
	template<class T, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	unsigned int StringToUInt( const T& i_Text );

	/// Convert a float to a string
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	T FloatToString( float i_Value );

	/// Convert a string to a float
#if (cxx_default_function_template_args == 1)
	template<class T, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	float StringToFloat( const T& i_Text );

	/// Convert a double to a string
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	T DoubleToString( double i_Value );

	/// Convert a string to a double
#if (cxx_default_function_template_args == 1)
	template<class T, class S = custom_stringstream_t>
#else
	template<class T, class S>
#endif
	double StringToDouble( const T& i_Text );

	/// Escape string appropriately for XML text
	template<class T>
	T EscapeXMLText( const T& i_Text );

	/// Escape string appropriately for XML attribute value
	template<class T>
	T EscapeXMLAttrib( const T& i_Text );

	/// Convert a string representation of hexadecimal digits to raw data bytes
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t>
#else
	template<class T>
#endif
	void StringToDataHex( unsigned char* i_Data, size_t i_Length, const T& i_HexString );

	/// Convert raw data bytes to a string representation of hexadecimal digits
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t>
#else
	template<class T>
#endif
	T DataToStringHex( const unsigned char* i_Data, size_t i_Length );

	/// Convert raw data bytes to a string representation of binary digits
#if (cxx_default_function_template_args == 1)
	template<class T = custom_string_t>
#else
	template<class T>
#endif
	T DataToStringBinary( const unsigned char* i_Data, size_t i_Length );

	/// Tokenize based on 1 or more single-character delimiters
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int tokenize( const T& i_Text, V& o_Tokens, const T& i_Delimiters, const int i_Limit = 0 );

	/// Same as tokenize, but tokenizes tokens in reverse order
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int tokenize_r( const T& i_Text, V& o_Tokens, const T& i_Delimiters, int i_Limit = 0 );

	/// Similar to Tokenize, but retains delimiter(s) on left side of each token
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int lsplit( const T& i_Text, V& o_Tokens, const T& i_Delimiters, const int i_Limit = 0 );

	/// Same as lsplit, but splits tokens in reverse order
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int lsplit_r( const T& i_Text, V& o_Tokens, const T& i_Delimiters, int i_Limit = 0 );

	/// Tokenize based on a single, 1-or-more character delimiter
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int explode( const T& i_Text, V& o_Tokens, const T& i_Delimiter, const int i_Limit = 0 );

	/// Same as explode, but tokenizes tokens in reverse order
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int explode_r( const T& i_Text, V& o_Tokens, const T& i_Delimiter, int i_Limit = 0 );

	/// Combine tokens using a single, 1-or-more character delimiter
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	T implode( const V& i_Tokens, const T& i_Delimiter = "", int i_Limit = 0 );

	/// Same as implode, but combines tokens in reverse order
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	T implode_r( const V& i_Tokens, const T& i_Delimiter = "", int i_Limit = 0 );

	// Extract all tokens between i_StartDelimiter and i_EndDelimiter
	template<class T, class V>	//NOTE: V should be a type like vector<T>
	int extract( T& i_Text, V& o_Tokens, const T& i_StartDelimiter, const T& i_EndDelimiter, int i_Limit = 0 );

	/// Parse a key-value pair, where key and value are separated by i_Joiner
#if (cxx_default_function_template_args == 1)
	template< class T, class M, class V = custom_vector_t<T> >	//NOTE: M should be a type like map<T,T>
#else
	template<class T, class M, class V>	//NOTE: M should be a type like map<T,T>
#endif
	int ParsePair( const T& i_Text, M& o_Pairs, const T& i_Joiner );

	/// Parse key-value pairs, where key and value are separated by i_Joiner and pairs are separated by i_Delimiter
#if (cxx_default_function_template_args == 1)
	template< class T, class M, class V = custom_vector_t<T> >	//NOTE: M should be a type like map<T,T>
#else
	template<class T, class M, class V>	//NOTE: M should be a type like map<T,T>
#endif
	int ParsePairs( const T& i_Text, M& o_Pairs, const T& i_Delimiter, const T& i_Joiner, int i_Limit = 0 );

	template<class T>
	T TimeToString( const tm& i_Time, const T& i_Format = "%Y-%m-%d %H:%M:%S" );

}

// Header-only Implementation
namespace KHP {

	/**
	 * @note Whitespace is defined as any character with an ASCII equivalent value
	 * less than or equal to 32.
	 * @param[in] i_Text text to search within
	 * @param[in] i_StartPos leftmost position within i_Text to search
	 * @param[in] i_EndPos 1 past rightmost position in i_Text to search, or 0 for
	 * the right end of i_Text
	 * @return position of leftmost whitespace, or string::npos if not found
	 */
	template<class T>
	typename T::size_type find_white( const T& i_Text, typename T::size_type i_StartPos, typename T::size_type i_EndPos ) {
		if( i_EndPos == 0 ) i_EndPos = i_Text.size();
			for( typename T::size_type i = i_StartPos; i < i_EndPos; ++i )
				if( i_Text[i] <= ' ' ) return i;

		return T::npos;
	}

	/**
	 * @note Whitespace is defined as any character with an ASCII equivalent value
	 * less than or equal to 32.
	 * @param[in] i_Text text to search within
	 * @param[in] i_StartPos 1 past rightmost position within i_Text to search, or
	 * 0 for the right end of i_Text
	 * @param[in] i_EndPos leftmost position in i_Text to search
	 * @return position of rightmost whitespace, or string::npos if not found
	 */
	template<class T>
	typename T::size_type find_white_r( const T& i_Text, typename T::size_type i_StartPos, typename T::size_type i_EndPos ) {
		if( i_StartPos == 0 ) i_StartPos = i_Text.size();
		--i_StartPos;
		for( typename T::size_type i = i_EndPos; i <= i_StartPos; ++i )
			if( i_Text[i_StartPos - i] <= ' ' ) return i_StartPos - i;

		return T::npos;
	}

	/**
	 * @note Non-whitespace is defined as any character with an ASCII equivalent
	 * value greater than 32.
	 * @param[in] i_Text text to search within
	 * @param[in] i_StartPos leftmost position within i_Text to search
	 * @param[in] i_EndPos 1 past rightmost position in i_Text to search, or 0 for
	 * the right end of i_Text
	 * @return position of leftmost whitespace, or string::npos if not found
	 */
	template<class T>
	typename T::size_type find_black( const T& i_Text, typename T::size_type i_StartPos, typename T::size_type i_EndPos ) {
		if( i_EndPos == 0 ) i_EndPos = i_Text.size();
		for( typename T::size_type i = i_StartPos; i < i_EndPos; ++i )
			if( i_Text[i] > ' ' ) return i;

		return T::npos;
	}

	/**
	 * @note Non-whitespace is defined as any character with an ASCII equivalent
	 * value greater than 32.
	 * @param[in] i_Text text to search within
	 * @param[in] i_StartPos 1 past rightmost position within i_Text to search, or
	 * 0 for the right end of i_Text
	 * @param[in] i_EndPos leftmost position in i_Text to search
	 * @return position of rightmost whitespace, or string::npos if not found
	 */
	template<class T>
	typename T::size_type find_black_r( const T& i_Text, typename T::size_type i_StartPos, typename T::size_type i_EndPos ) {
		if( i_StartPos == 0 ) i_StartPos = i_Text.size();
		--i_StartPos;
		for( typename T::size_type i = i_EndPos; i <= i_StartPos; ++i )
			if( i_Text[i_StartPos - i] > ' ' ) return i_StartPos - i;

		return T::npos;
	}

	/**
	 * @param[in] i_Text text to trim whitespace from left side of
	 * @return trimmed text
	 */
	template<class T>
	T ltrim( const T& i_Text ) {
		typename T::size_type tSPos = find_black( i_Text );
		if( tSPos == T::npos ) return T();
		return i_Text.substr( tSPos, i_Text.size() - tSPos );
	}

	/**
	 * @param[in] i_Text text to trim whitespace from right side of
	 * @return trimmed text
	 */
	template<class T>
	T rtrim( const T& i_Text ) {
		typename T::size_type tEPos = find_black_r( i_Text );
		if( tEPos == T::npos ) return T();
		return i_Text.substr( 0, tEPos + 1 );
	}

	/**
	 * @param[in] i_Text text to trim whitespace from both sides of
	 * @return trimmed text
	 */
	template<class T>
	T trim( const T& i_Text ) {
		typename T::size_type tSPos = find_black( i_Text );
		typename T::size_type tEPos = find_black_r( i_Text );
		if( tSPos == T::npos || tEPos == T::npos ) return T();
		return i_Text.substr( tSPos, tEPos + 1 - tSPos );
	}

	/**
	 * @param[in] i_Text text to convert to uppercase
	 * @return uppercase-converted text
	 */
	template<class T>
	T ucase( const T& i_Text ) {
		T tResult( i_Text.length(), 0 );
		for( typename T::size_type i = 0; i < i_Text.length(); ++i )
			tResult[i] = toupper( i_Text[i] );

		return tResult;
	}

	/**
	 * @param[in] i_Text text to convert to lowercase
	 * @return lowercase-converted text
	 */
	template<class T>
	T lcase( const T& i_Text ) {
		T tResult( i_Text.length(), 0 );
		for( typename T::size_type i = 0; i < i_Text.length(); ++i )
			tResult[i] = tolower( i_Text[i] );

		return tResult;
	}

	template<class T>
	bool IsAlpha( const T& i_Text, typename T::size_type i_Start, typename T::size_type i_Length ) {
		typename T::size_type tStop = i_Text.length();
		if( i_Length != T::npos && i_Length > 0 ) {
			tStop = i_Start + i_Length;
			if( tStop > i_Text.length() ) tStop = i_Text.length();
		}
		for( typename T::size_type i = i_Start; i < tStop; ++i )
			if( !isalpha( i_Text[i] ) ) return false;

		return true;
	}

	template<class T>
	bool IsAlphaNum( const T& i_Text, typename T::size_type i_Start, typename T::size_type i_Length ) {
		typename T::size_type tStop = i_Text.length();
		if( i_Length != T::npos && i_Length > 0 ) {
			tStop = i_Start + i_Length;
			if( tStop > i_Text.length() ) tStop = i_Text.length();
		}
		for( typename T::size_type i = i_Start; i < tStop; ++i )
			if( !isalnum( i_Text[i] ) ) return false;

		return true;
	}

	template<class T, class S>
	int StringToInt( const T& i_Text ) {
		if( i_Text.empty() ) return 0;
		S s;
		s << i_Text;
		int i;
		s >> i;
		return i;
	}

	template<class T, class S>
	T IntToString( int i_Value ) {
		S s;
		s << i_Value;
		return s.str();
	}

	template<class T, class S>
	unsigned int StringToUInt( const T& i_Text ) {
		if( i_Text.empty() ) return 0;
		S s;
		s << i_Text;
		unsigned int i;
		s >> i;
		return i;
	}

	template<class T, class S>
	T UIntToString( unsigned int i_Value ) {
		S s;
		s << i_Value;
		return s.str();
	}

	template<class T, class S>
	float StringToFloat( const T& i_Text ) {
		S s;
		s << i_Text;
		float f;
		s >> f;
		return f;
	}

	template<class T, class S>
	T FloatToString( float i_Value ) {
		S s;
		s << i_Value;
		return s.str();
	}

	template<class T, class S>
	double StringToDouble( const T& i_Text ) {
		S s;
		s << i_Text;
		double f;
		s >> f;
		return f;
	}

	template<class T, class S>
	T DoubleToString( double i_Value ) {
		S s;
		s << i_Value;
		return s.str();
	}

	template<class T>
	T EscapeXMLText( const T& i_Text ) {
		T t = i_Text;
		size_t p = 0;
		while( (p = t.find_first_of( '&', p )) != T::npos ) {
			t.replace( p, 1, "&amp;" );
			p += 5;
		}
		p = 0;
		while( (p = t.find_first_of( '<', p )) != T::npos ) {
			t.replace( p, 1, "&lt;" );
			p += 4;
		}
		p = 0;
		while( (p = t.find_first_of( '>', p )) != T::npos ) {
			t.replace( p, 1, "&gt;" );
			p += 4;
		}
		return t;
	}

	template<class T>
	T EscapeXMLAttrib( const T& i_Text ) {
		T t = i_Text;
		size_t p = 0;
		while( (p = t.find_first_of( '&', p )) != T::npos ) {
			t.replace( p, 1, "&amp;" );
			p += 5;
		}
		p = 0;
		while( (p = t.find_first_of( '"', p )) != T::npos ) {
			t.replace( p, 1, "&quot;" );
			p += 6;
		}
		p = 0;
		while( (p = t.find_first_of( '<', p )) != T::npos ) {
			t.replace( p, 1, "&lt;" );
			p += 4;
		}
		p = 0;
		while( (p = t.find_first_of( '>', p )) != T::npos ) {
			t.replace( p, 1, "&gt;" );
			p += 4;
		}
		return t;
	}

	template<class T>
	void StringToDataHex( unsigned char* i_Data, size_t i_Length, const T& i_HexString ) {
		size_t OutIdx = 0;
		int Digits = 0;
		uint8_t v = 0;
		for( size_t i = 0; i < i_HexString.size() && OutIdx < i_Length; ++i ) {
			const char h = i_HexString[i];
			if( h >= '0' && h <= '9' ) {
				v <<= 4;
				v += h - '0';
				++Digits;
			} else if( h >= 'A' && h <= 'F' ) {
				v <<= 4;
				v += 10 + h - 'A';
				++Digits;
			} else if( h >= 'a' && h <= 'f' ) {
				v <<= 4;
				v += 10 + h - 'a';
				++Digits;
			} else {
				if( Digits ) Digits = 2;
			}
			if( Digits > 1 ) {
				i_Data[OutIdx] = v;
				++OutIdx;
				Digits = 0;
				v = 0;
			}
		}
		if( Digits && OutIdx < i_Length )
			i_Data[OutIdx] = v;
	}

	template<class T>
	T DataToStringHex( const unsigned char* i_Data, size_t i_Length ) {
		static const char* a_hex = "0123456789ABCDEF";
		T hex( i_Length * 2, char( 0 ) );
		for( size_t i = 0; i < i_Length; ++i ) {
			hex[i * 2] = a_hex[i_Data[i] >> 4];
			hex[i * 2 + 1] = a_hex[i_Data[i] & 0x0F];
		}
		return hex;
	}

	template<class T>
	T DataToStringBinary( const unsigned char* i_Data, size_t i_Length ) {
		T bin( i_Length * 8, char( 0 ) ) ;
		for( size_t i = 0; i < i_Length; ++i ) {
			int d = i_Data[i];
			for( int b = 0; b < 8; ++b ) {
				bin[i * 8 + b] = (d & 0x80) ? '1' : '0';
				d <<= 1;
			}
		}
		return bin;
	}

	template<class T, class V>
	int tokenize( const T& i_Text, V& o_Tokens, const T& i_Delimiters, const int i_Limit ) {
		typename T::size_type lastPos = 0;
		typename T::size_type pos = i_Text.find_first_of( i_Delimiters, lastPos );

		int count = 0;
		while( i_Limit == 0 || count < i_Limit ) {
			if( T::npos == pos ) {
				o_Tokens.push_back( i_Text.substr( lastPos ) );
				return count + 1;
			}

			o_Tokens.push_back( i_Text.substr( lastPos, pos - lastPos ) );
			count++;
			lastPos = pos + 1;
			pos = i_Text.find_first_of( i_Delimiters, lastPos );
		}

		return count;
	}

	template<class T, class V>
	int tokenize_r( const T& i_Text, V& o_Tokens, const T& i_Delimiters, int i_Limit ) {
		V f_Tokens;
		int c = tokenize( i_Text, f_Tokens, i_Delimiters );
		if( i_Limit == 0 || c < i_Limit ) i_Limit = c;
		--c;
		for( int i = 0; i < i_Limit; ++i )
			o_Tokens.push_back( f_Tokens[c - i] );

		return i_Limit;
/*
		typename T::size_type lastPos = i_Text.size() - 1;
		typename T::size_type pos = i_Text.find_last_of( i_Delimiters, lastPos );

		int count = 0;
		while( i_Limit == 0 || count < i_Limit ) {
			if( T::npos == pos ) {
				o_Tokens.push_back( i_Text.substr( 0, lastPos + 1 ) );
				return count + 1;
			}

			o_Tokens.push_back( i_Text.substr( pos + 1, lastPos - pos ) );
			count++;
			lastPos = pos - 1;
			pos = i_Text.find_last_of( i_Delimiters, lastPos );
		}

		return count;
*/
	}

	template<class T, class V>
	int lsplit( const T& i_Text, V& o_Tokens, const T& i_Delimiters, const int i_Limit ) {
		typename T::size_type lastPos = 0;
		typename T::size_type pos = i_Text.find_first_of( i_Delimiters, lastPos + 1 );

		int count = 0;
		while( i_Limit == 0 || count < i_Limit ) {
			if( T::npos == pos ) {
				o_Tokens.push_back( i_Text.substr( lastPos ) );
				return count + 1;
			}

			o_Tokens.push_back( i_Text.substr( lastPos, pos - lastPos ) );
			count++;
			lastPos = pos;
			pos = i_Text.find_first_of( i_Delimiters, lastPos + 1 );
		}

		return count;
	}

	template<class T, class V>
	int lsplit_r( const T& i_Text, V& o_Tokens, const T& i_Delimiters, int i_Limit ) {
		V f_Tokens;
		int c = lsplit( i_Text, f_Tokens, i_Delimiters );
		if( i_Limit == 0 || c < i_Limit ) i_Limit = c;
		--c;
		for( int i = 0; i < i_Limit; ++i ) {
			o_Tokens.push_back( f_Tokens[c - i] );
		}
		return i_Limit;
	}

	template<class T, class V>
	int explode( const T& i_Text, V& o_Tokens, const T& i_Delimiter, const int i_Limit ) {
		typename T::size_type lastPos = 0;
		typename T::size_type pos = i_Text.find( i_Delimiter, 0 );

		int count = 0;
		while( i_Limit == 0 || count < i_Limit ) {
			if( T::npos == pos ) {
				o_Tokens.push_back( i_Text.substr( lastPos ) );
				return count + 1;
			}

			o_Tokens.push_back( i_Text.substr( lastPos, pos - lastPos ) );
			count++;
			lastPos = pos + i_Delimiter.size();
			pos = i_Text.find( i_Delimiter, lastPos );
		}

		return count;
	}

	template<class T, class V>
	int explode_r( const T& i_Text, V& o_Tokens, const T& i_Delimiter, int i_Limit ) {
		V f_Tokens;
		int c = explode( i_Text, f_Tokens, i_Delimiter );
		if( i_Limit == 0 || c < i_Limit ) i_Limit = c;
		--c;
		for( int i = 0; i < i_Limit; ++i ) {
			o_Tokens.push_back( f_Tokens[c - i] );
		}
		return i_Limit;
/*
		typename T::size_type lastPos = 0;
		typename T::size_type pos = i_Text.find( i_Delimiter, 0 );

		if( T::npos == pos ) {
			o_Tokens.push_back( i_Text );
			return 1;
		}

		int count = 0;
		while( (pos != T::npos) && (i_Limit == 0 || count < i_Limit) ) {
			o_Tokens.insert( o_Tokens.begin(), i_Text.substr( lastPos, pos - lastPos ) );
			count++;
			lastPos = pos + i_Delimiter.size();
			pos = i_Text.find( i_Delimiter, lastPos );
		}

		if( i_Text.size() > lastPos ) {
			o_Tokens.insert( o_Tokens.begin(), i_Text.substr( lastPos, i_Text.size() - lastPos ) );
			count++;
		}

		return count;
*/
	}

	template<class T, class V>
	T implode( const V& i_Tokens, const T& i_Delimiter, int i_Limit ) {
		if( !i_Tokens.size() ) return T();

		if( i_Limit == 0 ) i_Limit = int( i_Tokens.size() );

		T s( i_Tokens[0] );
		for( int i = 1; i < i_Limit; ++i )
			s += i_Delimiter + i_Tokens[i];

		return s;
	}

	template<class T, class V>
	T implode_r( const V& i_Tokens, const T& i_Delimiter, int i_Limit ) {
		if( !i_Tokens.size() ) return T();

		int max = int( i_Tokens.size() ) - 1;
		if( i_Limit == 0 ) i_Limit = int( i_Tokens.size() );
		i_Limit = int( i_Tokens.size() ) - i_Limit;

		T s( i_Tokens[max] );
		for( int i = max - 1; i >= i_Limit; --i )
			s += i_Delimiter + i_Tokens[i];

		return s;
	}

	template<class T, class M, class V>
	int ParsePair( const T& i_Text, M& o_Pairs, const T& i_Joiner ) {
		V kv;
		int l = explode( i_Text, kv, i_Joiner, 2 );
		if( kv[0] != "" ) {
			if( l > 1 )
				o_Pairs[kv[0]] = kv[1];
			else
				o_Pairs[kv[0]] = "";
		}
		return l;
	}

	template<class T, class M, class V>
	int ParsePairs( const T& i_Text, M& o_Pairs, const T& i_Delimiter, const T& i_Joiner, int i_Limit ) {
		V p;
		int t = explode( i_Text, p, i_Delimiter, i_Limit );
		if( i_Limit && t > i_Limit ) t = i_Limit;
		for( int i = 0; i < t; ++i )
			ParsePair<T,M,V>( p[i], o_Pairs, i_Joiner );

		return t;
	}

	template<class T, class V>
	int extract( T& i_Text, V& o_Tokens, const T& i_StartDelimiter, const T& i_EndDelimiter, int i_Limit ) {
		int count = 0;

		typename T::size_type spos = 0;
		spos = i_Text.find( i_StartDelimiter, spos );
		while( (T::npos != spos) && (i_Limit == 0 || count < i_Limit) ) {
			typename T::size_type epos = i_Text.find( i_EndDelimiter, spos );
			if( T::npos == epos ) epos = i_Text.size();

			o_Tokens.push_back( i_Text.substr( spos + i_StartDelimiter.size(), epos - spos - i_StartDelimiter.size() ) );
			i_Text.erase( spos, epos - spos + i_EndDelimiter.size() );
			count++;
			
			spos = i_Text.find( i_StartDelimiter, spos );
		}
		
		return count;	
	}

	template<class T>
	T TimeToString( const tm& i_Time, const T& i_Format ) {
		char buffer[256];
		strftime( buffer, 256, i_Format.c_str(), &i_Time );
		return T( buffer );
	}

}

#endif // ifndef _KHPtech_StringUtil_h
